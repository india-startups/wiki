# India Startup Wiki

You can start with the wiki [index](index.md).

## Consuming

This is wip. For now:

1. Options
   1. If you know `git` then you can clone the repo and read it locally
   2. If you don't know `git` then you can using gitlab to read content.
2. Gaps
   1. No search
   2. Gitlab UI not designed for this use case

## Contributing

Standard pull-requests work just fine.

## Feedback and Request For Content

1. You can [comment on any change](https://gitlab.com/india-startups/wiki/-/commit/2ef03436ff11d9b6ad89a01bd4fe8baa6087b216)
2. You can request additional content by opening a ticket at [Gitlab issues](https://gitlab.com/india-startups/playbooks-wiki/-/issues)

## Licence 

<a rel="license" href="http://creativecommons.org/licenses/by-nc/3.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc/3.0/88x31.png" /></a>

This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nc/3.0/">Creative Commons Attribution-NonCommercial 3.0 Unported License</a>

## Copyright

Copyright 2021 Sidu Ponnappa and Shobhit Srivastava.
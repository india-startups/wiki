# India Startup Wiki

This is an [open source repository](https://gitlab.com/india-startups/wiki) of notes and playbooks for common problems faced by folks in the India startup ecosystem.

> “In theory there is no difference between theory and practice – in practice there is”
>
> \- Yogi Berra.

This wiki does not aim to define "one correct way." Competing playbooks are welcome.

## Index

1. Recruiting
   1. [Market](recruiting/market.md)
   2. [Sourcing](./recruiting/sourcing.md)
   3. [Operations](recruiting/operations.md)
      1. [Running Recruiting](recruiting/operations/running-recruiting.md)
   4. [Team](recruiting/team.md)
   5. [Interviews](recruiting/interviews.md)
2. Org design
   1. [Product Org](org/product-org.md)
3. People 
   1. Goal setting
   2. Performance
   3. Incentives

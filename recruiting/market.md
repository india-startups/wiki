# Market [wip]

[Home](../index.md)

In which we capture the realities of the startup tech hiring market in India.

### Raw Tree

1. Geography
   1. it's all one global hiring market now with a [trimodal spread](https://blog.pragmaticengineer.com/software-engineering-salaries-in-the-netherlands-and-europe/amp/) (ty [@gandharva](https://twitter.com/gandharva/status/1375308514650775555))
      1. 3 categories of competition
         1. Benchmarking against local competing companies
         2. Competing against ALL local companies
         3. Competing against ALL regional/global companies
      2.  "average" salary for software engineers is no longer a meaningful mentric, just an average salary per one of the three, distinct categories
      3. Indian startups are now competing with SEA startups for the same local talent pool
   2. FAANG scale tech cos have already been hiring globally for several years
      1. silver lining - you know you’re doing things right if you’re losing people to FAANG sf/london/berlin offices :P
      2. FAANG started early because they had the muscle to deal with visas to bring in talent
      3. now that remote work is becoming a real thing, the barrier to global hiring is dramatically lower, so more and more companies are hiring talent wherever they can find them because they don't have to relocate them anymore
2. Demand side
   1. Two segments, segmented by the reason for hiring
      1. companies hiring for survival i.e without these people the buisness will fail (Usually Seed to C or bootstrapped)
      2. companies hiring for optionality i.e without these people the business will explore fewer options outside the core product offering (usually Series C+)
   2. geographically, highest demand originates from India + SEA based funded startups
   3. funding rising exponentially - 450 rounds of Series A+ in 2019 in India alone
   4. unicorns being minted every month on average now
3. Supply side
   1. Four segments, segmented by experience and responsibility (this does not mean _years_ of experience, though that is one proxy)
      1. grad - no portfolio, no professional experience
      2. junior - some portfolio, no serious responsibilities
      3. mid - more portfolio, some serious responsibilities
         1. Examples
            1. own P&L
            2. own budget
            3. own production release rate
            4. own uptime
            5. own hiring targets
            6. own timelines/scope
      4. sr - industry veteran with a diverse portfolio and experience dealing with a wide range of serious responsibilies

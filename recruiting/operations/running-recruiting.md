# Running Recruiting
{:.no_toc}

[Home](../../index.md)

If you are a founder who has recently started hiring in tech, this page aims to share the
mental models that are relevant to _operationally_ running recruiting that I've developed over the years that I've been doing this.

I'm staying focused on concepts relevant to the day-to-day _running_ of a recruiting team and pipeline, and am going to minimise digressions into strategy.
This means I'm going to provide little or no explanation of _why_ I see things this way. Trying to do so has left me spinning my wheels these last few months as
I write page after page of theory. I figure it's better I unblock myself and ship this operational playbook, and the rest will get done when it gets done.

I should also call out that all of my hiring experience is in the India/SEA region. Some or all of this advice may not be applicable to other hiring markets.

As always, YMMV.

## Index
{:.no_toc}

1. 
{:toc}

## Audience

The advice in this post isn't for everyone, so I'm going to strive to clarify the segments to which any given piece of
advice applies. This advice only applies to companies that are:

1. hiring for [Craft](https://en.wikipedia.org/wiki/Craft) positions - programmers, product managers, designers, data
   scientists etc.
2. hiring for their "product division", not their "IT department" if you know what I mean
3. hiring the [best talent](https://twitter.com/ponnappa/status/1534058703451607040) they can afford

A direct implication of these criteria is that so-called volume hiring, which I define roughly as `> 20 hires per month` is operationally intractable under [current hiring market conditions](https://sidu.in/wiki/startups/recruiting/market.html) unless you are paying obscenely above-market salaries.

If you are looking to do `< 1 hire per month` on average then I don't believe that the RoI on building and running a hiring pipeline is justified.
At volumes `< 1 per month`, it's more pragmatic to invest in low-cost social-media-driven content marketing and to use this to drive serendipitous hiring rather than take on the CapEx of building a pipeline. 

The exceptions to this are venture-funded startups that are post-PMF and scaling at 10% MoM or more. Building a hiring pipeline takes many quarters of sustained effort, and such companies _have_ to front-load on doing so or risk being severely shorthanded a few months down the line. 


## Lifecycle

Whoever runs Recruiting owns an operational funnel which I've described below. The labels used for the stages vary in the industry - these are the ones that I favour.

#### Stage-wise metrics

|| Stage | Outcome | Quantity Metrics | Quality Metrics |
|| ----------- | ----------- | ----------- | ----------- |
|1| [Sourcing](#sourcing) | Generates Leads | CAC: <br>- $/Lead <br><br>#/Cycle Time: <br>- Leads/Day <br>- Leads/Week <br>- etc. | Conversion ratios: <br>- Contact:Lead <br>- Lead:Prospective Employee <br>- Lead:Employee |
|2| [Lead qualification](#lead-qualification) | Converts Leads to Prospects | CAC: <br>- $/Prospect <br><br>- #/Cycle Time | Conversion ratios: <br>- Lead:Prospect <br>- Prospect:Prospective Employee <br>- Prospect:Employee |
|3| [Candidature](#candidature) | Converts Prospects into Candidates | - #/Cycle Time | Conversion ratios: <br>- Prospect:Candidate <br>- Prospect:Prospective Employee <br>- Prospect:Employee |
|4| [Interview](#interview) | Converts Candidates into Qualified Candidates | - #/Cycle Time <br>- # of panellists | - Interview Panel Utilisation <br>- Candidate Reschedules <br>- Interviewer Reschedules <br>- P50 & P90 wait time for next interview <br><br>Conversion ratio: <br>- Prospect:Prospective Employee |
|5| [Offer](#offer) | Converts Qualified Candidates into Prospective Employees | #/Cycle Time | - P50 & P90 candidate wait time for offer <br><br>Conversion ratio: <br>- Candidate:Prospective Employee |
|6| [Join](#join) | Converts Prospective Employees into Employees | #/Cycle Time | Conversion ratio: <br>- Prospective Employee:Employee |
|7| [Onboard](#onboard) | Converts Employees into Tenured Employees | #/Cycle Time | Conversion ratio: <br>- Employees: Tenured Employees |

#### Global metrics

- End-to-end cycle time from [Sourcing](#sourcing) to [Offer](#offer)
  - Tells us the operational rigour, should be 2-4 weeks, ideally
- End-to-end conversion from [Sourcing](#sourcing) to [Join](#join)
  - Tells us the quality of the sourcing
- End-to-end conversion from [Interview](#interview) to [Onboard](#onboard)
  - Tells us the quality of interviews
- Number of referrals from rejected candidates
  - Tells us the quality of the candidate experience
- Number of +ve and -ve posts on Glassdoor related to interviews
  - Self-explanatory

## Sourcing

Sourcing is a _discovery_ and _identification_ problem. It requires designing and running a process that can

1. find people (`Contacts`) in the market who broadly match the open positions we have
2. establish that reliable means of communication with them exist
3. create an up-to-date profile of the person

A `Contact` isn't a `Lead` until all three steps have been completed.

### Channels

The first rule of sourcing in tech is "whenever we want to hire, the people we want to hire are seldom actively looking for a
change." As a consequence, there is no one proven channel through which we can source.

Identifying, prioritising and investing into developing channels is at the heart of sourcing. Most of the `Sourcing` metrics in the table above can (and should) be broken down per channel.

At the highest level of abstraction, channels can be classified into _outbound_ and _inbound_ channels. In terms of activities, outbound channels involve sales-type activities while inbound channels involve marketing-type activities.

#### Outbound sourcing

Outbound sourcing involves using offline and online networks to generate `Leads`. It will involve a lot of selling of the role because most of the folks identified this way won't be looking out.
At the time of writing, the single most popular outbound sourcing channel is LinkedIn Recruiter, which turns LinkedIn's social graph into a searchable database of candidates.
In my experience, however, the single most _valuable_ channel is employee referrals. It's worth making investments into employee referrals with the objective of making the process easy, pleasant and worthwhile for all participants.

Other, high-quality channels exist like Twitter, Open Source networks, StackOverflow etc. - but they require relatively higher effort on the part of the person doing the sourcing.
LinkedIn Recruiter reduces the problem to mindless keyword matching which is what makes it popular.

The `Lead:Prospect` conversion ratio for outbound channels should be relatively high because those doing the sourcing have a clear set of signals to look for when sourcing.
It should also improve over time as feedback from subsequent stages of the funnel drives the tuning of sourcing signals.

#### Inbound sourcing

Inbound sourcing involves using a combination of content marketing, traditional advertising and performance marketing to attract the right kind of folks to apply. The precise mix depends on your budget and the marketing skills of your team.

Here's an example of a content marketing video we used to build up Gojek's inbound hiring pipeline for PM, Design and Engineering positions back in 2018. 'The Hard Copy' did a [case study](https://thehardcopy.co/how-gojek-attracted-talent-in-india/) on Gojek's inbound strategy if you're interested in more detail.

<div class="embed">
<iframe width="560" height="315" src="https://www.youtube.com/embed/9p-wZG3RXqs" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</div>

Inbound channels suffer from a lot of noise at the top of the funnel. Most incoming `Contacts` will be junk applicants, and filtering out the noise to identify `Leads` is a largely manual process that can be quite painful to run.
The advantage, however, is that inbound channels scale extremely well, and are highly complementary to outbound channels.

The `Lead:Prospect` conversion ratio for inbound channels will always be extremely high simply because everyone applying is actually looking out for a change.
Spinning up an inbound channel can dramatically improve `Lead:Prospect` and `Prospect:Candidate` conversion on existing outbound channels simply because folks can find out more about both the company and the role.

If the brand built through inbound is sufficiently strong, you will find converting `Prospects` into `Candidates` becomes effortless. You should also experience a dramatic reduction in `Prospect -> Candidate Cycle Time`.

### Accountability

Individuals doing outbound sourcing are accountable for their personal `Leads/Day`, `Leads/Week` etc. metrics and the health of the `Lead:Prospect` and `Lead:Employee` ratios for the `Leads` they source.

The inbound team is accountable for `Leads/Day`, `Leads/Week` etc. metrics for each inbound channel, and the health of the `Lead:Prospect` and `Lead:Employee` ratios for those channels.

The Head of Recruiting is accountable for these cumulatively across all channels, as well as the `$/Lead` and `$/Prospect` CAC metrics. These metrics should be broken down and tracked per recruiter involved in sourcing.

## Lead Qualification

This stage of the process converts `Leads` into `Prospects`. The simplest way to do this is to create a checklist of signals to look for across a combination of the person's resume,
online footprint and most importantly for craft hiring, any work they have in the public domain such as open source.

It's important to note that at small scales, the qualification process is owned by the hiring manager because they have an intuitive, tacit understanding of the relevant signals for a given open position.
But as the system is scaled up, qualification will need to be handled by in-house or contract recruiters who may not share that tacit knowledge and will need the checklist to ensure that `Lead:Prospect` ratios stay healthy.

If `Lead:Prospect` ratios become unhealthy, it will result in an increase in `Interview Panel Utilisation` without a commensurate improvement in `# of Employees/Cycle time`.

### Accountability

Anyone who qualifies leads - usually recruiters or hiring managers - is accountable for their personal `Lead:Prospect` and `Prospect:Employee` ratio.

The Head of Recruiting is accountable for the overall `Lead:Prospect` and `Prospect:Employee` ratios.

## Candidature

This stage of the process converts `Prospects` into `Candidates`.
It involves recruiters and hiring managers reaching out to `Prospects` and convincing them to participate in the interview process for a particular open position.

The conversion metrics for this stage are heavily influenced by the company's employer brand in general and the hiring manager's personal brand in particular.
The `Prospect:Candidate` ratio of one's outbound channels is one of the best proxy metrics to judge one's employer brand.

The level of warmth and hospitality delivered to `Leads` and `Prospects` when recruiters and hiring managers engage with them also is a significant factor.

A coherent narrative for the company, the function and for specific open positions is crucial to improving this metric. Every open position should have the recruiting equivalent of
a pre-sales toolkit - a document that defines these narratives, and acts as a knowledge base for recruiters and hiring managers to help them sell the role better.

### Accountability

The hiring manager is accountable for developing the parts of the pre-sales toolkit relevant for a specific open position.
The Head of Function (typically a VP or CXO) is accountable for developing the narrative for their function.
The Founder/CEO is accountable for developing the company narrative.

The Head of Recruiting is accountable for ensuring that all pre-sales toolkit narratives are coherent across the abstractions of position, function and company, and for the health of the conversion ratio.

## Interview

Interview stages convert `Candidates` into `Qualified Candidates`. Designing and running interview loops requires a sharp focus on developing a pool of interviewers and ensuring that they are not overloaded, or worse,
wasting their time rejecting low-quality `Leads` who should never have made it to the `Candidate` stage.

It requires a fair bit of operational muscle to coordinate, schedule and reschedule interviews as the availability of candidates and panellists changes.
Having specialists - usually called _Recruiting Coordinators_ on the recruiting team whose focus is scheduling and coordination can help with this.

Avoid [artisanal interviewing](https://sidu.in/wiki/startups/recruiting/interviews.html), coach panellists to focus on assessing candidates rather than asserting status and ensure high levels of hospitality
from recruiters and panellists to candidates both while scheduling as well as during interviews.

Any candidate worth hiring always has options, so being courteous and friendly at every stage of the process is table stakes.

A key metric to track here is `# of referrals from rejected candidates`. This is a referral channel that is the simplest, surest proxy for a great hiring process.

The stages of the interview loop should be split into _filtration_ stages and _selection_ stages.

### Filtering

Filtration stages exist to ensure that low-quality candidates are detected and rejected with the aim of minimising panel load. They should be simple to schedule and take very little time and effort from panellists.

In markets with very high fraud rates, i.e where candidates routinely exaggerate or misrepresent their capabilities, filtration stages are critical to ensuring that legitimate candidates get the attention they deserve without panels burning out.

Filtration stages should be laser-focused on [fizz-buzz evaluations](https://wiki.c2.com/?FizzBuzzTest). The idea is to assess in the cheapest, quickest manner possible if a particular candidate exceeds the _lower bar_ for an open position.
These stages should also be tuned to ensure that viable candidates aren't being filtered due to a tendency to naively and quickly judge for upper-bound criteria. That should be the focus of the selection stages.

### Selecting

Selection stages exist to ensure that candidates meet or exceed the _upper bar_ for an open position.
The upper bar can never be objectively defined for craft hiring, but it should be subjectively defined using interview templates that include [standardised question banks and scoring](https://socraticowl.com/post/hire-like-the-israeli-military/).

Creating a [shared understanding among panellists of the upper bar](https://www.gojek.io/blog/from-subjective-interviews-to-binary-hires-a-story) and intentionally evolving that definition is crucial to getting this right.

### Accountability

The design of interview loops and the development of a pool of panellists is the responsibility of the Head of Function, usually a VP or CXO.

The operational aspects of scheduling and coordinating interviews is the responsibility of the Head of Recruiting.

## Offer

The offer stage converts `Qualified Candidates` into `Prospective Employees`. This usually involves zeroing in on a compensation package that is acceptable to a `Qualified Candidate`, and will lead to them signing an offer letter.

Compensation guidelines for recruiters and hiring managers to use when making offers are essential to speeding up the process.

### Accountability

Finance and the Head of Function are jointly responsible for designing a compensation framework. If the org is large enough to have an HR function, HR will also be involved.
The hiring manager is accountable for negotiating and making offers.

## Join

The process reaches its conclusion when a `Prospective Employee` actually shows up for work, converting them into `Employees`. In hot markets, it's not unusual for `Prospective Employees` who have
already signed an offer letter to renege on it because they subsequently receive another offer that they consider superior.

Engaging `Prospective Employees` right up to their joining dates is crucial to the health of the relevant conversion metrics.

### Accountability

The hiring manager is accountable for engaging `Prospective Candidates` from the time they accept an offer until they join.

## Onboard

An ugly truth that we all hide from in recruiting is that the process isn't done when a candidate joins - it's done only when the candidate is proven to be a good fit in practice.

We can define this milestone in many ways, but I prefer the operational simplicity of defining it as `Completes Two Consecutive Half-Yearly Review Cycles` without being categorised as an underperformer. 

### Accountability

The reporting manager is accountable for ensuring `Employees` convert into `Tenured Employees` and also ensuring that the recruiting database is kept up-to-date in this regard.

## Hiring Bars

Keep in mind that there is _always_ a pool of candidates at almost any level of hiring bar. An inability to generate sufficiently high-quality `Leads` (i.e with a high conversion ratio to `Qualified Candidates`)
is an indicator of under-performance in a recruiter. The bottleneck is rarely in the availability of `Leads` (super specialised roles are exceptions); it is almost always in the inability to pay enough to convert `Leads` to `Candidates`.
<div class="embed">
<blockquote class="twitter-tweet"><p lang="en" dir="ltr">Hiring bars in tech can be counterintuitive.<br><br>Intuitive:<br>Low skill=Low impact, High skill=High impact, linear-ish curve i.e <br><br>Mediocre talent=Medium impact on metrics.<br><br>Actuality:<br>Low skill=Negative impact on metrics, power curve i.e <br><br>Mediocre talent is often NEGATIVE impact. <a href="https://t.co/00JnLuWEYc">pic.twitter.com/00JnLuWEYc</a></p>&mdash; Sidu Ponnappa (@ponnappa) <a href="https://twitter.com/ponnappa/status/1534058703451607040?ref_src=twsrc%5Etfw">June 7, 2022</a></blockquote> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>

A competent Head of Recruiting will never justify failure by saying "Our bar is too high." Doing so is a red flag.

Instead, they will say one of 

- "We aren't paying enough" and prove this by either 
  - showing a poor `Prospect:Candidate` conversion ratio clearly attributable to insufficient comp and/or a weak/negative employer brand, or 
  - by having a high `Prospect:Qualified Candidate` ratio and a poor `Qualified Candidate:Prospective Employee` ratio with `Qualified Candidates` rejecting offers due to insufficient comp
- "We have too many false negatives" and prove this by showing a poorly defined interview process and dramatically varying results at the Interview stage depending on the interviewer
- "We have too long a cycle time on stage x and we are losing candidates as a consequence" and prove this by showing stage-wise cycle-time data

## On Recruiters and Hiring Managers

A Head of Recruiting role is very close to a hybrid sales/ops leadership role. The fizzbuzz for a competent Head of Recruiting is the ability to describe their ideal design for such a system in considerable detail.
They will also have a long list of complaints regarding the limitations of the ATS software they have used in the past.

For IC recruiters and for hiring managers, an unforgeable signal indicating capability is a deep understanding of the traits of high-quality candidates of a given specialisation, and the ability to clearly articulate the signals that indicate high-quality
as well as the signals that indicate low-quality (and possibly fraudulent) `Contacts`, `Leads` and `Candidates`. They know the personas, know their preferences and know where such folk hang out both online and offline.

While generating volumes of quality `Leads` isn't a necessary skill for hiring managers, the inability to generate even low single-digit high-quality `Leads` every quarter is a massive red flag.

Lastly, the performance assessments of folks doing sourcing (both recruiters and hiring managers) should be strongly influenced by the health of their individually attributed sourcing metrics, and they should have access
to the reports that will drive this assessment. The approach here should be similar to how a sales team is run.

## Optimising

Improving these numbers requires careful instrumentation of recruiting processes, both online and offline. The challenge here cannot be overstated.
Simply ensuring that all participants log events and update statuses in a timely manner is a non-trivial problem.

You should choose an ATS that supports the logging of all these events and can report all of these metrics on a daily, weekly, monthly and quarterly basis. You should also be able to attribute specific metrics to channels or individuals where appropriate.
Personally, I have not had much success with commercial ATS software - none of them seems to be designed with an [operations research](https://en.wikipedia.org/wiki/Operations_research) point-of-view
and, to me, have always proven to be very painful bottlenecks in scaling up my hiring.

Given that no ATS I know of can do what is necessary, I would recommend designing your system on top of a combination of

- an off-the-shelf ATS with strong data export capabilities
- a tabular system like Google Sheets
- a metrics capable kanban process flow system like Jira
- a documentation system like Google Docs

Yes, this will be an ugly kludge of a system, but I don't see any alternative on the horizon.

This system has to be backed by a rigorous, cadenced audit loop and a religious focus on stage-wise cycle-time and conversion ratios. 
Recruiters, hiring managers and panellists involved in the process should have easy access to up-to-date metrics that are relevant to them.

# Team

[Home](../index.md)

There are 4 broad problem areas, which can be covered by the same person, or split into specialist roles. You can experiment with splitting these responsibilities, but do ensure that the responsibilities are set in terms of metrics.

1. [Sourcing](./sourcing.md)
   1. Strategy
      1. Outbound
      2. Inbound
   2. Metrics
      1. Lead to Offer ratio
      2. Lead to Accept ratio
2. Operations
   1. Strategy
      1. It's an Operations Research Problem
   2. Metrics
      1. Re-scheduled interviews (this is a huge source of wasted effort)
         1. Number of interviews per unit time where candidate rescheduled
         2. Number of interviews per unit time where panelist rescheduled
      2. End to end cycle time of a candidate from lead to offer
      3. Number of candidates currently experiencing broken SLAs (eg. promised candidate a response in 48 hours, but not reached out)
3. Hospitality (aka customer experience aka candidate experience)
   1. Strategy
      1. Given the extreme gap between demand/supply in favour of supply, candidate experience and employee experience become of paramount difference - after, of course, compensation.
         1. Key differentiators from other prospective employers are essential
            1. Big brand (like consumer products)
            2. Compensation
            3. Employee experience
               1. Candidate experience
         2. So called "White-glove service" is essential to candidate experience becoming a differentiator
   2. Metrics
      1. Number of negative Glassdoor reviews about interview process
      2. Candidate NPS type survey rating
      3. %age of candidates with perfect SLA experience (i.e we kept all our operational promises)
4. Comp Negotiation
   1. Metrics
      1. Stage cycle time
      2. Number of offer rejections citing comp as reason rolling 



## Building a recruiting team

### Hiring Experienced Recruiters

Generally speaking most of the tech recruiting industry in the region is oriented toward IT services. Both individual recruiters and agencies have experience hiring large volumes (100s or 1000s of hires/month) at low(er) costs and with very high conversion rates.

Hiring for low volume, low conversion rate in tech is fundamentally a different skillset.



## Misc

Operations needs to be taken very seriously as a cross-cutting concern (also see [Interview Pipelines](./interview-pipelines))

Be very intentional about the role of the hiring manager. There is always a hiring manager, even if not formally specified. The hiring manager is the person who wants the hire to happen. In startups, it's often the CEO. Be clear which metrics your hiring managers are accountable for vs recruiting team members.